package com.movchan.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

public class FileReader {
    private final static Logger LOGGER = LogManager.getLogger(FileReader.class);


    public static String reader(String path) throws IOException {
        InputStream is = null;
        try {
            is = new FileInputStream(path);
        }catch (FileNotFoundException e){
            LOGGER.error("file not found");
        }
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        String line = reader.readLine();
        StringBuilder sb = new StringBuilder();
        while (line != null){
            sb.append(line).append("\n");
            line = reader.readLine();
        }
        return sb.toString();
    }
}
