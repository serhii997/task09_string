package com.movchan.model;

import java.util.ArrayList;
import java.util.List;

public class Condition {
    private String text;
    private List<Word> wordList = new ArrayList<>();

    public Condition(String text) {
        this.text = clearText(text);
        toList(this.text);
    }

    private String clearText(String text){
        text = text.replaceAll("[():;!?,=—]", "");
        text = text.replaceAll("[/]", " ");
        return text;
    }

    private void toList(String text){
        for (String wordText : text.split("[\\s]+")) {
            String word = wordText.trim();
            if (word.length() > 0) {
                wordList.add(new Word(word));
            }
        }
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<Word> getWordList() {
        return wordList;
    }

    public void setWordList(List<Word> wordList) {
        this.wordList = wordList;
    }

    @Override
    public String toString() {
        return  text;
    }
}
