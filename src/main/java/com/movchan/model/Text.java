package com.movchan.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Text {
    private final static Logger LOGGER = LogManager.getLogger(FileReader.class);

    private String text;
    private List<Condition> stringList = new ArrayList<>();

    public Text() {
        outputText();
    }

    private void outputText(){

        try {
            text = FileReader.reader("folder/aboutJava.txt");
            for (String condition : text.trim().split("[.!?]")) {
                stringList.add(new Condition(condition));
            }
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
        //stringList.stream().forEach(System.out::println);
//        findTheLargestNumberSentencesTheSameWords();
//        System.out.println("bla");
//        printSentencesOrderByCountWords();
    }

    public void output(){
        stringList.stream().forEach(System.out::println);
    }

    public void printSentencesOrderByCountWords() {
        Map<Integer, Integer> countWordInSentences = getCountWordInSentences();
        countWordInSentences.entrySet()
                .stream()
                .sorted(Comparator.comparingInt(Map.Entry::getKey))
                .forEach((map) -> LOGGER.info(map.getKey() + " "
                        + "\t\t"
                        + map.getValue()));
    }

    private Map<Integer, Integer> getCountWordInSentences() {
        Map<Integer, Integer> countWordInSentences = new LinkedHashMap<>();
        for (int i = 1; i < stringList.size(); i++) {
            countWordInSentences.put(stringList.get(i).getWordList().size(), i);
        }
        return countWordInSentences;
    }

    public void findTheLargestNumberSentencesTheSameWords() {
        List<Map<Word, Integer>> listWordsCount = new ArrayList<>();
        Map<Word, Integer> wordCount;
        for (Condition condition : stringList) {
            wordCount = new LinkedHashMap<>();
            for (Word word : condition.getWordList()) {
                if (word.getText().length() != 0) {
                    //compute()
                    Integer integer = wordCount.get(word);
                    wordCount.put(word, integer == null ? 1 : integer + 1);
                }
            }
            listWordsCount.add(wordCount);
        }
        getMaxWordEachSentence(listWordsCount);
    }

    private void getMaxWordEachSentence(List<Map<Word, Integer>> listWordsCount) {
        listWordsCount.forEach(map->{
            Map.Entry<Word, Integer> stringIntegerEntry = map.entrySet().stream()
                    .max(Comparator.comparing(Map.Entry::getValue))
                    .orElse(null);
            LOGGER.info(stringIntegerEntry.getKey().getText()
                    + " - " + stringIntegerEntry.getValue());
        });
    }

    public void findUniqWord(){
        List<Word> firstWord = new ArrayList<>(stringList.get(0).getWordList());

        for (int i = 1; i < stringList.size(); i++) {
            for (Word word: stringList.get(i).getWordList()) {
                firstWord.remove(word);
            }
        }
        firstWord.forEach(word -> LOGGER.info(word.getText()));
    }

}
