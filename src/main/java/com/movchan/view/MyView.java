package com.movchan.view;

import com.movchan.model.FileReader;
import com.movchan.model.Text;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.*;

public class MyView {
    private final static Logger LOGGER = LogManager.getLogger(FileReader.class);

    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    private ResourceBundle bundle;
    private ResourceBundle bundleList;
    private static ResourceBundle bundleWords;
    private Text text = new Text();

    private Locale locale;

    public MyView() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("MyMenu",locale);
        setMenu();
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", (() -> text.output()));
        methodsMenu.put("2", this::internationalizeMenuUkraine);
        methodsMenu.put("3", this::internationalizeMenuRusia);
        methodsMenu.put("4", this::internationalizeMenuEnglish);
        methodsMenu.put("5", (() -> text.findTheLargestNumberSentencesTheSameWords()));
        methodsMenu.put("6", (() -> text.printSentencesOrderByCountWords()));
    }

    public void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("5", bundle.getString("5"));
        menu.put("6", bundle.getString("6"));
    }

    private void internationalizeMenuUkraine(){
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        printMenu();
    }

    private void internationalizeMenuEnglish(){
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        printMenu();
    }

    private void internationalizeMenuRusia(){
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        printMenu();
    }

    public static String bundleWord(String key) {
        return bundleWords.getString(key);
    }

    private void printMenu() {
        menu.forEach((key, value) -> System.out.println("`"+key+"` - "+"\t"+value));
//        LOGGER.info("\n" + bundleWords.getString("choosecommand"));
    }
    private void bla(){
        System.out.println("fsdfdsfdsf");
    }

    public void run(){
        String input ="";
        printMenu();
        do {
            try {
                input = MyView.input.next().toLowerCase();
                methodsMenu.get(input).print();
            } catch (NullPointerException e) {
                LOGGER.error(bundleWords.getString("wrongcommand"));
                printMenu();
            } catch (Exception e) {
                LOGGER.error(e.getMessage());
            }
        } while (!input.equalsIgnoreCase("Q"));
    }
}
